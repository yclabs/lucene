package com.hardnoxjava.lucenedemo.index.directory;

import java.io.IOException;

import org.apache.lucene.store.Directory;

public interface ISearchIndexDirectoryImpl {
	public Directory getDirectoryImplementation() throws IOException;
}
