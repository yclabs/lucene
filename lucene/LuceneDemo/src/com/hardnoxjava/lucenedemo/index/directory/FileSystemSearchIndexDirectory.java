package com.hardnoxjava.lucenedemo.index.directory;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class FileSystemSearchIndexDirectory implements ISearchIndexDirectoryImpl{
	private String indexLocation = "/tmp/lucene/test/fsdir";
	private Directory directory;
	
	@Override
	public Directory getDirectoryImplementation() throws IOException {
		if(directory == null){
			directory = FSDirectory.open(new File(indexLocation));
		}
		
		return directory;
	}

	public void setIndexLocation(String indexLocation) {
		this.indexLocation = indexLocation;
	}

}
