package com.hardnoxjava.lucenedemo.index.directory;

import java.io.IOException;

import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

public class RAMSearchIndexDirectory implements ISearchIndexDirectoryImpl{
	private Directory directory;
	
	@Override
	public Directory getDirectoryImplementation() throws IOException {
		if(directory == null){
			directory = new RAMDirectory();
		}
		
		return directory;
	}
	public Directory getDirectoryAsCopy(Directory otherDirectory) throws IOException{
		return new RAMDirectory(otherDirectory);
	}
}
