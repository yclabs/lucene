package com.hardnoxjava.lucenedemo.index.directory;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;

public class IndexDirectoryFactory {
	public static Directory getRAMDirectory(){
		return new RAMDirectory();
	}
	public static Directory getRAMDirectory(Directory otherDir) throws IOException{
		return new RAMDirectory(otherDir);
	}
	public static Directory getFSDirectory(String directoryPath) throws IOException{
		return FSDirectory.open(new File(directoryPath));
	}
}
