package com.hardnoxjava.lucenedemo.reader;

import java.io.IOException;
import java.io.InputStream;

import net.htmlparser.jericho.CharacterReference;
import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.MasonTagTypes;
import net.htmlparser.jericho.MicrosoftConditionalCommentTagTypes;
import net.htmlparser.jericho.PHPTagTypes;
import net.htmlparser.jericho.Source;

public class HtmlFileReader extends FileReader implements IFileReader{
	
	public HtmlFileReader(InputStream contentStream) {
		this.contentStream = contentStream;
	}

	@Override
	public String getText() {
		MicrosoftConditionalCommentTagTypes.register();
		PHPTagTypes.register();
		PHPTagTypes.PHP_SHORT.deregister(); // remove PHP short tags for this example otherwise they override processing instructions
		MasonTagTypes.register();
		try {
			Source source=new Source(contentStream );
			title = getTitle(source);
			return (source.getTextExtractor().setIncludeAttributes(true).toString());
		} catch (IOException e) {
			logger.error(e);
			return null;
		}

	}
	
	private String getTitle(Source source) {
		Element titleElement=source.getFirstElement(HTMLElementName.TITLE);
		if (titleElement==null) 
			return null;
		// TITLE element never contains other tags so just decode it collapsing whitespace:
		return CharacterReference.decodeCollapseWhiteSpace(titleElement.getContent());
	}

}
