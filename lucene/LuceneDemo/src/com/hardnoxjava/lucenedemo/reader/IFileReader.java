package com.hardnoxjava.lucenedemo.reader;

import java.io.IOException;

public interface IFileReader {
	public String getText() throws IOException;
	
	public void closeStream();

	public String getTitle();
}
