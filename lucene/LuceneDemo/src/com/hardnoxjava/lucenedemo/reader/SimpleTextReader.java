package com.hardnoxjava.lucenedemo.reader;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.CharBuffer;

public class SimpleTextReader extends FileReader implements IFileReader {

	public SimpleTextReader(InputStream contentStream) {
		this.contentStream = contentStream;
	}

	@Override
	public String getText() {
		String txt="";
		try {
			DataInputStream in = new DataInputStream(contentStream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			// Read File Line By Line

			while ((strLine = br.readLine()) != null) {
				logger.debug(strLine);
				txt += strLine;
			}

			// Close the input stream
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return txt;
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return null;
	}

}
