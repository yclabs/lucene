package com.hardnoxjava.lucenedemo.reader;

import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;

public abstract class FileReader{
	protected InputStream contentStream;
	protected Logger logger = Logger.getLogger(FileReader.class);
	
	protected String title;

	public String getTitle(){
		return title;
	}
	public void closeStream(){
		if(contentStream != null){
			try {
				contentStream.close();
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}
}
