package com.hardnoxjava.lucenedemo.reader;

import java.io.IOException;
import java.io.InputStream;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

public class PDFFileReader extends FileReader  implements IFileReader{

	public PDFFileReader(InputStream contentStream) {
		this.contentStream = contentStream;
	}

	@Override
	public String getText() throws IOException {
		PDFParser parser = new PDFParser(contentStream);  
		parser.parse();  
		
		COSDocument cd = parser.getDocument();
		
		PDFTextStripper stripper = new PDFTextStripper();
		PDDocument pdDoc = new PDDocument(cd);
		title = pdDoc.getDocumentInformation().getTitle();
		String text = stripper.getText(pdDoc); 
		logger.debug(title);
		cd.close();
		return text;
	}

	@Override
	public String getTitle() {		
		if(title == null){
			try{
				PDFParser parser = new PDFParser(contentStream);  
				parser.parse();  
				
				COSDocument cd = parser.getDocument();
				
				PDDocument pdDoc = new PDDocument(cd);
				title = pdDoc.getDocumentInformation().getTitle();
				
				cd.close();
			}catch (Exception e) {
				title = null;
			}
		}
		return title;
	}

}
