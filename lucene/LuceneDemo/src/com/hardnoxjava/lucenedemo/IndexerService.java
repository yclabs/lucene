package com.hardnoxjava.lucenedemo;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;

import com.hardnoxjava.lucenedemo.data.model.LuceneSearchResult;
import com.hardnoxjava.lucenedemo.index.directory.ISearchIndexDirectoryImpl;
import com.hardnoxjava.lucenedemo.index.directory.IndexDirectoryFactory;
import com.hardnoxjava.lucenedemo.reader.HtmlFileReader;
import com.hardnoxjava.lucenedemo.reader.IFileReader;
import com.hardnoxjava.lucenedemo.reader.PDFFileReader;
import com.hardnoxjava.lucenedemo.reader.SimpleTextReader;

/*
 * @author Milos Stoiljkovic
 * 
 * Service for executing core Lucene indexing operations
 * 
 */
public class IndexerService {
	private IndexWriter writer;
	private IndexWriter fsWriter;
	private IndexReader reader;
	private Directory fsDir;
	private Directory indexDir;

	
	private ISearchIndexDirectoryImpl searchIndexDirectoryImpl;
	
	private static Logger logger = Logger.getLogger(IndexerService.class);
	
	private IFileReader fileReader;

	public enum DocumentFileType {
		 XML
		,HTML
		,PDF
		,TXT
		,DOC
		,RTF
	}
	
	private final static String CONTENTS_FIELD = "CONTENTS";
	private final static String UUID_FIELD = "UUID";
	private final static String TITLE_FIELD = "TITLE";
	
	private static String indexLocation = "D:/lucene/lucene_index";
	
	public void setSearchIndexDirectoryImpl(
			ISearchIndexDirectoryImpl searchIndexDirectoryImpl) {
		this.searchIndexDirectoryImpl = searchIndexDirectoryImpl;
	}
	
	
	
	/**
	 * Loads search index from configured location
	 * 
	 * @param createNew
	 * @throws IOException
	 */
	public void initIndex() throws IOException{
		indexDir = IndexDirectoryFactory.getRAMDirectory();
				
		Analyzer analyser = new StandardAnalyzer(Version.LUCENE_30);
		
		//working index
		IndexWriterConfig indexConfig = new IndexWriterConfig(Version.LUCENE_30, 
				analyser);
		indexConfig.setOpenMode(OpenMode.CREATE);
		writer = new IndexWriter(indexDir, indexConfig);

	}
	public void initFSIndex(boolean createNew) throws IOException{
		fsDir = IndexDirectoryFactory.getFSDirectory(indexLocation);
		//here we use StandardAnalyser for the document
		Analyzer analyser = new StandardAnalyzer(Version.LUCENE_30);
		IndexWriterConfig indexFSConfig = new IndexWriterConfig(Version.LUCENE_30, 
				analyser);
		
		if(createNew){
			indexFSConfig.setOpenMode(OpenMode.CREATE);
		}else{
			indexFSConfig.setOpenMode(OpenMode.CREATE_OR_APPEND);
		}
		fsWriter = new IndexWriter(fsDir, indexFSConfig);
		
	}
	public void mergeToFS() throws IOException{
		fsWriter.addIndexes(indexDir);
		writer.close();
		initIndex();
		fsWriter.optimize();
		fsWriter.commit();
	}
	public void commitIndex() throws CorruptIndexException, IOException{
		if(writer!=null){
			writer.commit();
		}
	}
	public void closeIndex() throws CorruptIndexException, IOException{
		if(reader != null){
			reader.close();
		}
		if(writer!=null){		
			writer.close();
		}
		if(fsWriter!=null){		
			fsWriter.close();
		}
	}
	public void optimizeIndex() throws IOException{
		initFSIndex(false);
		fsWriter.optimize();
		fsWriter.close();
	}
	public void optimizeIndex(int maxNumberOfSegments) throws IOException{
		initFSIndex(false);
		fsWriter.optimize(maxNumberOfSegments);
		fsWriter.close();
	}
	public void indexFile(InputStream istream, String uuid, DocumentFileType docFileType) throws IOException{
		if(istream == null){
			throw new IllegalArgumentException("Passed InputStream parameter to indexFile method is null!");
		}
		if(uuid == null){
			throw new IllegalArgumentException("Passed String uuid parameter to indexFile method is null!");
		}
		
			switch(docFileType){
			case HTML:
				fileReader = new HtmlFileReader(istream);
				break;
			case PDF:
				fileReader = new PDFFileReader(istream);
				break;
			case TXT:
				fileReader = new SimpleTextReader(istream);
				break;
			default:
				fileReader = new SimpleTextReader(istream);
				break;
			}
		
		String text;
		try {
			text = fileReader.getText();
		} catch (IOException e) {
			throw e;
		}
		logger.debug(text);
		String title = fileReader.getTitle();
		logger.debug(title);
		//we get uuid as parameter
		
		//Lucene :
		Document doc = new Document();
		
		StringReader inputReader = new StringReader(text);
					
		Field fieldContent = new Field(CONTENTS_FIELD, inputReader);			
		doc.add(fieldContent);
		
		if(title != null){
			Field fieldTitle = new Field(TITLE_FIELD, title, Field.Store.YES, Field.Index.ANALYZED);
			doc.add(fieldTitle);
		}
		
		Field fieldUUID = new Field(UUID_FIELD, uuid, Field.Store.YES, Field.Index.NOT_ANALYZED);
		doc.add(fieldUUID);
		
		writer.addDocument(doc);
		
		logger.info("doc indexed");
		
	}
	
	public void removeDocumentFromIndex(String uuid) throws IOException {
		writer.deleteDocuments(new Term(UUID_FIELD, uuid));
		
		logger.info("Deleted document with UUID: " + uuid);
		
	}
	public List<LuceneSearchResult> search(String titleKeywords, boolean titleMustOccur, 
			String contentKeywords, boolean contentMustOccur) throws IOException{
		Directory indexDir = IndexDirectoryFactory.getFSDirectory(indexLocation);
		
		IndexSearcher isearch = new IndexSearcher(indexDir);
		
		
		/**
		 * TopScoreDocCollector collects results from index search
		 * Maximum capacity is here fixed to 10000
		 */
		TopScoreDocCollector collector = 
			  TopScoreDocCollector.create(10000, true);

		BooleanQuery queryTitle = null;
		
		if(titleKeywords != null && titleKeywords.trim().length() > 0){
			queryTitle = getMultiTermQuery(titleKeywords, TITLE_FIELD
					, titleMustOccur,false);
		}
		
		BooleanQuery queryContents = null;
		if(contentKeywords != null && contentKeywords.trim().length() > 0){
			queryContents = getMultiTermQuery(contentKeywords, CONTENTS_FIELD
					, contentMustOccur,false);
		}

		//final query = combined queries for fields
		BooleanQuery finalQuery = new BooleanQuery();
		
		/**
		 * We can use MUST and SHOULD parameters to make AND/OR conditions
		 * Different types of queries can be created and combined depending of content we search
		 * and needed conditions
		 */
		if(queryTitle != null){
			finalQuery.add(queryTitle, Occur.MUST);
		}
		if(queryContents != null){
			finalQuery.add(queryContents, Occur.MUST);
		}

		isearch.search(finalQuery, collector);
		//i sda je collector objekat napunjen
		int totalHits = collector.getTotalHits();
		
		ScoreDoc[] hits;
		hits = collector.topDocs().scoreDocs;
		
		//sada na osnovu dobijene Lucene kolekcije pakujemo DTO objekta koje vracamo gore na interfejs
		List<LuceneSearchResult> ret = extractSearchResults(isearch, hits, totalHits);
		
		return ret;	
	}
	private BooleanQuery getMultiTermQuery(String queryString, String fieldName, boolean mustOccur, boolean leadingWildcard){
		BooleanQuery bquery = new BooleanQuery();
		
		
		StringTokenizer strTokenizer = new StringTokenizer(queryString);
		//make conditions for every keyword
		while(strTokenizer.hasMoreElements()){
			String token = strTokenizer.nextToken();
			if(leadingWildcard){
				token = "*"+token;
			}
			token += "*";			
			//Fuzzy query finds similar items			
			//Query tq = new FuzzyQuery(new Term(fieldName, token.toLowerCase()),0.6f,token.length()-1); 
			//Basic TermQuery
			//TermQuery(new Term(fieldName, token.toLowerCase()));
			//Wildcard query: supports ?, * etc. wildcards
			Query tq = new WildcardQuery(new Term(fieldName, token.toLowerCase()));
			//sa interfejsa se prosledjuje mustOccur parametar, odnosno da li se zahtevaju sve reci, ili bilo koja od reci
			//sklapa se upit i za cirilicne i latinicne
			if(mustOccur){
				bquery.add(tq, Occur.MUST);
			}else {
				bquery.add(tq, Occur.SHOULD);
			}
		}
		
		return bquery;
	}
	private List<LuceneSearchResult> extractSearchResults(IndexSearcher is,
			ScoreDoc[] hits, int totalHits) throws CorruptIndexException, IOException {
		List<LuceneSearchResult> ret = new ArrayList<LuceneSearchResult>();
		
		//kupimo vrednosti odgovarajucih polja iz indeksa
	    for(int i=0;i<hits.length;++i) {
	      int docId = hits[i].doc;
	      Document d = is.doc(docId);
	      
	      String title = d.get(TITLE_FIELD);
	      String uuid = d.get(UUID_FIELD);
	      
	      LuceneSearchResult res = new LuceneSearchResult(uuid, title);
		 
		 ret.add(res);
	    }
		return ret;
	}

}
