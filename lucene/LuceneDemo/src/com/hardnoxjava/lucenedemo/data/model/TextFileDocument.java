package com.hardnoxjava.lucenedemo.data.model;

public class TextFileDocument extends BasicDocument {
	private String path;
	
	public String getPath() {
		return path;
	}

	public TextFileDocument(String fullPath){
		this.path = fullPath;
	}
}
